# laravel-express
Repositorio de estudo e testes laravel.

Banco usado, sqlite.
- Aplique o comando:
  php artisan serve

# Acesse os links

Link 1: http://localhost:8000/ola/{digite um nome}

Link 2: <a href="http://localhost:8000/notas">Notas</a>

Link 3: <a href="http://localhost:8000/blog">Blog</a>
