
@extends('template')

@section('content')

<h1>Blog</h1>
    @foreach($posts as $post)
        <h3>{{ $post->title }} <i>( {{ $post->created_at }} )</i> </h3>
        <p>{{ $post->content }}</p>
        <b>Tags: </b> <br/>
        <ul>
            @foreach($post->tags as $tag)
            <li>{{$tag->name}}</li>
            @endforeach
        </ul>

        <h4>Comments</h4>
        @foreach($post->comments as $comment)
        <b>Name: </b> {{$comment->name}}
        <b>Comment: </b> {{$comment->comment}}
        @endforeach
<hr>
@endforeach

@endsection


